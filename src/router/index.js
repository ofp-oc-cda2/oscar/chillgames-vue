import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import Register from '../views/Register.vue';
import Login from '../views/Login.vue';
import Profile from '../views/Profile.vue';
import Games from '../views/Games.vue';
import PageNotFound from '../views/PageNotFound.vue';
import JoinOrCreateRoom from '../views/JoinOrCreateRoom.vue';
import Room from '../views/Room.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile
  },
  {
    path: '/games',
    name: 'Games',
    component: Games
  },
  {
    path: '/room/:id',
    name: 'joinOrCreate',
    component: JoinOrCreateRoom
  },
  {
    path: '/room/:gamename/:id',
    name: 'Room',
    component: Room
  },
  { 
    path: "/:catchAll(.*)",
    name: "Page 404",
    component: PageNotFound 
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router