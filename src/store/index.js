import { createStore } from 'vuex'

export default createStore({
  state: {
    username: '',
    profilePicture: '',
    mail: '',
    userData: null,
    party: null,
  },
  mutations: {
    setUsername(state, username) {
      state.username = username
    },
    setProfilePicture(state, profilePicture) {
      state.profilePicture = profilePicture
    },
    setMail(state, mail) {
      state.mail = mail
    },
    getUserData(state, userData) {
      state.userData = userData
    },
    setDateCreate(state, dateCreate) {
      state.userData.dateCreate = dateCreate
    },
    setParty(state, party) {
      state.party = party
    },
  },
  actions: {
  },
  modules: {
  }
})
