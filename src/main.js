import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import PrimeVue from 'primevue/config';
import Dialog from 'primevue/dialog';
import moshaToast from 'mosha-vue-toastify';
import 'mosha-vue-toastify/dist/style.css';

import 'primevue/resources/themes/saga-blue/theme.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import '/node_modules/primeflex/primeflex.css';


createApp(App)
    .use(PrimeVue)
    .use(moshaToast)
    .use(router)
    .use(store)
    .component('Dialog', Dialog)
    .mount('#app');
